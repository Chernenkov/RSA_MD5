#include <time.h>
#include <cmath>
#include <stdlib.h>
#include <unistd.h>

using namespace std;	

#include "generator.h"

bool isPrime(unsigned long number)
{
	unsigned long sqr = sqrt(number);
	// if(sqr > 4000000000) return false; // wont check numbers with sqrt() over 4 billion
	for(unsigned long i = 2; i <= sqrt(number) + 1; i++) {
		if ((number % i) == 0) return false;
	}
	return true;
}

unsigned long genPrime() {
	unsigned int sleep_time = 0; // delay for time(0) - our seed to change
	
	srand(time(0));
	sleep(sleep_time);
	int n1 = rand();
	
	sleep(sleep_time);
	srand(time(0));
	int n2 = rand();
	
	sleep(sleep_time);
	srand(time(0));
	int n3 = rand();	
	
	unsigned long result = n1+n2+n3;

	while(!isPrime(result)) {
		srand(time(0));
		sleep(sleep_time);
		n1 = rand();
		sleep(sleep_time);
		srand(time(0));
		n2 = rand();
		sleep(sleep_time);
		srand(time(0));
		n3 = rand();
		result = n1+n2+n3;
	}
	return result;
}