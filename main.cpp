#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "generator.cpp"
using namespace std;

unsigned long greatestCommonDivisor(unsigned long a, unsigned long b);
unsigned long calculateE(unsigned long eiler);
unsigned long calculateD(unsigned long e, unsigned long eiler);

unsigned long greatestCommonDivisor(unsigned long a, unsigned long b)
{
	unsigned long temp;
	while(a > 0) 
	{
		temp = a;
		a = b % a;
		b = temp;
	}

	return b; 
}

unsigned long calculateE(unsigned long eiler) 
{
	// e: (1 < e < eiler(n))
	// GCD(e, eiler(n)) = 1
	unsigned long e;
	unsigned long gcd;
	for(e = 2; e < eiler; e++) 
	{
		gcd = greatestCommonDivisor(e, eiler);
		if(gcd == 1 && e > 10000)
		{
			return e;
		} 	
	}

	return -1;
}

unsigned long calculateD(unsigned long e, unsigned long eiler) 
{
	// find mult. reverse 
	// d*e = 1 mod eiler(n)
	unsigned long d;
	unsigned long k = 1;

	while(true) 
	{
		k += eiler;
		if ((k % e) == 0)
		{
			d = k / e;
			return d;
		}
	}
}


int main()
{
	// cout << genPrime();
	unsigned long p = 0;
	unsigned long q = 0;
	unsigned long n = 0;
	unsigned long eiler = 0;
	unsigned long e = 0;
	unsigned long d = 0;
	// ask or generate p and q
	short ask = 0;
	cout << "\n 1. Enter prime numbers p and q \n 2. Generate prime numbers p and q" << endl;
	cin >> ask ;
	system("clear");
	if(ask == 1)
	{
		system("clear");
		cout << "\nEnter p: ";
		cin >> p;
		while(!isPrime(p)) 
		{
			cout << "\nWrong number: entered number is not prime!" << endl;
			cin >> p;
		}
		cout << "\nEnter q: ";
		cin >> q;
		while(!isPrime(q)) 
		{
			cout << "\nWrong number: entered number is not prime!" << endl;
			cin >> q;
		}
		n = p*q;
		// cout << "\nEntered prime numbers: \np = " << p << "\nq = " << q << endl;
		// cout << "n = p*q = " << n;
	}
	if (ask == 2)
	{
		// cout << "Please, wait while numbers are being generated...";
		p = genPrime();
		q = genPrime();
		n = p * q;
		// system("clear");
		// cout << "\nGenerated prime numbers: \np = " << p << "\nq = " << q << endl;
		// cout << "n = p*q = " << n;
	}
	// count eiler function for n
	eiler = (p - 1) * (q - 1);
	// calculate E
	e = calculateE(eiler);
	// calculate D
	d = calculateD(e, eiler);
	system("clear");
	cout << "System parameters: " << "\np = " << p << "\nq = " << q << "\nn = " << n << endl;
	cout << "eiler(n) = " << eiler << "\ne = " << e << "\nd = " << d << endl; 
	// public: {e,n}
	// privat: {d, n}
	cout << "\nPrivate key: {" << d << "," << n << "}";
	cout << "\nPublic key: {" << e << "," << n << "}" << endl;
}